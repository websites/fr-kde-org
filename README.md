This repository holds the content of fr.kde.org

## Building the website

### Install Jekyll

Instruction for Kubuntu 13.04:

    sudo apt-get install rubygems libruby-dev
    sudo gem install jekyll

More info [http://jekyllrb.com][]

Make sure to have UTF-8 locales before using jekyll :
    export LC_ALL=en_US.UTF-8
    export LANG=en_US.UTF-8


### Run Jekyll

From within this folder:

    jekyll serve --watch

Look at the result on <http://localhost:4000>.

## Publishing

KDE server infrastructure will run Jekyll a few minutes after you push changes
to master.

