---
layout: page
title: La communauté KDE
---

## Suivre l'actualité de KDE

- [dot.kde.org](http://dot.kde.org) : Actualités de la communauté KDE.
- [planetkde.org](http://planetkde.org) : Aggrégation de blogs de membres de la communauté KDE.
- [fr.planetkde.org](http://fr.planetkde.org) : La même chose, mais pour les blogs rédigés en français.

## Les blogs des contributeurs KDE

- [ervin](http://ervin.ipsquad.net/)
- [morice](http://morice.ipsquad.net/blog/)
- [Laurent](http://www.aegiap.eu/kdeblog/)
- [Ben](http://blog.ben2367.fr)
- [David](https://blogs.kde.org/blogs/dfaure)
- [Tim](http://timotheegiet.com/blog)
- [Carl](https://carlschwan.eu)

## Obtenir de l'aide

### Le forum

Le [forum KDE Francophone][forum] est un endroit convivial où se retrouvent de nombreux utilisateurs pour s'entraider et échanger des idées.

### IRC

Si vous préférez les discussions en direct, vous pouvez nous retrouver sur le réseau IRC Freenode, canal [#kde-fr][irc].

## Participer

Nous disposons de plusieurs listes de diffusion.

- [kde-events-fr][] : Cette liste est utilisée pour coordonner l'organisation d'évènements KDE dans les pays francophones. C'est ici que sont organisés des évènements comme aKademy-fr, les ateliers KDE de Toulouse, Solution Linux...

- [kde-francophone][] : Cette liste est utilisée par l'équipe de traduction des applications et espaces de travail KDE en français.

- [kde-fr-announce][] : Cette liste a pour but la promotion de KDE. Vous y retrouverez les annonces concernant les futures versions, mais également les évènements auxquels participera la communauté KDE.

[irc]: http://webchat.freenode.net/?channels=kde-fr
[forum]: http://forum.kde.org/fr
[kde-events-fr]: https://mail.kde.org/mailman/listinfo/kde-events-fr
[kde-francophone]: https://mail.kde.org/mailman/listinfo/kde-francophone
[kde-fr-announce]: https://mail.kde.org/mailman/listinfo/kde-fr-announce
