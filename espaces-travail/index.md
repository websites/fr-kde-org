---
layout: page
title: Les espaces de travail proposés par KDE
---

## Les espaces de travail

La communauté KDE propose plusieurs espaces de travail, afin de s'adapter au mieux à la machine utilisée.

### Plasma Desktop

"Plasma Desktop" est un espace de travail classique, souvent appelé le "Bureau".

L'apparence par défaut est simple et sera familière à tout utilisateur venant d'un autre environnement :

[![Plasma Desktop par défaut](thumb-plasma-desktop-defaut.png)](plasma-desktop-defaut.png)

"Plasma Desktop", comme tous les espaces de travail KDE, est facilement personnalisable au gré de vos besoins et de vos envies. L'espace de travail est constitué d'un ensemble de composants graphiques que vous pouvez déplacer, retirer, configurer.

[![Un bureau différent](thumb-plasma-desktop-custom.png)](plasma-desktop-custom.png)

De nombreux composants supplémentaires sont disponible par défaut, mais il est également possible d'en télécharger de nouveaux.

[![Téléchargement de nouveaux composants](thumb-plasma-desktop-ghns.png)](plasma-desktop-ghns.png)

### Plasma Netbook

Cet environnement de travail est adapté aux ordinateurs disposant d'une faible résolution. Tout est mis en œuvre pour donner le maximum d'espace aux applications : la barre horizontale en haut de l'écran est très fine et les barres de titre des fenêtres maximisées sont cachées : des boutons apparaissent en haut à droite de l'écran pour réduire ou fermer la fenêtre.

[![La page d'accueil de Plasma Netbook](thumb-plasma-netbook.png)](plasma-netbook.png)

[![Le navigateur web sur Plasma Netbook](thumb-plasma-netbook-konq.png)](plasma-netbook-konq.png)

<!--
### Plasma Active

Bureau adapté aux écrans tactiles.

[plasma-active.org](http://www.plasma-active.org)

### Plasma Media Center

Bureau adapté à l'utilisation sur votre télévision.
-->
