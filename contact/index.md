---
layout: default
title: Contact
---

Ce site est maintenu par la communauté francophone de KDE.

Vous pouvez nous contacter sur notre liste de diffusion : [kde-events-fr][].

[kde-events-fr]: https://mail.kde.org/mailman/listinfo/kde-events-fr