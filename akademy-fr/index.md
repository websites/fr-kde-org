---
layout: page
title: Akademy-fr
---

# Akademy-fr

Akademy-fr est la principale conférence consacrée à KDE en France. Son but est avant tout de permettre aux contributeurs francophones de se retrouver mais également de permettre la promotion de KDE en France. Pour les novices, il s'agit d'une occasion rêvée pour découvrir cet environnement libre et complet. Les personnes souhaitant contribuer pourront trouver tous types de contributeurs afin de mieux se lancer dans le grand bain de KDE. Vous y retrouverez à la fois des conférences orientées vers la contribution et vers le grand public.


## Akademy-fr 2014

Akademy France se déroulera les 15 et 16 novembre à Toulouse à l'ENSEEIHT au sein du [Capitole du libre](http://2014.capitoledulibre.org/). Des conférences sur prévues sur les thèmes suivants : 

- les nouveautés de KDE : Plasma 5, KDE-Connect et le Plasma Media Center
- le logiciel éducatif [gCompris](http://gcompris.net) et sa récente conversion aux technologies Qt / KDE.
- comment devenir contributeur KDE, que l'on soit développeur ou non !
- le manifeste de KDE : comment souder une communauté autours de valeurs communes
- Qt sur Android
- l'intégration continue mise en oeuvre par KDE
- le logiciel de dessin Krita
- l'artisanat comme modèle pour rechercher la qualité dans les logiciels libre

## Akademy-fr 2013

L'édition 2013 se déroulera les samedi 23 et dimanche 24 novembre 2013 à l'École Nationale Supérieure d'Électrotechnique, d'Électronique, d'Informatique, d'Hydraulique et des Télécommunications (ENSEEIHT) dans le centre de Toulouse.

[Plus d'information sur le site de Toulibre](http://toulibre.org/akademyfr-2013)

## Editions précédentes

* [Akademy-fr 2012](http://toulibre.org/akademyfr-2012)
