---
layout: post
title:  "KDE signe le Manifeste des Données Utilisateur 2.0"
date:   2015-11-22 00:00:00
categories: KDE
---
KDE, par le biais de son entité juridique KDE e.V., est l'une des organisations à l'initiative du Manifeste des Données Utilisateur 2.0 ([User Data Manifesto 2.0][1]) dont elle est l'un des premiers signataires. Ce manifeste définit les droits fondamentaux des utilisateurs concernant la protection de leurs données personnelles sur internet :

- le contrôle de l’accès aux données des utilisateurs
- la transparence sur la façon dont sont stockées les données
- la liberté dans le choix de la plate-forme.


[Lydia Pintscher][2], présidente de KDE e.V., explique : « Le quotidien de notre monde moderne dépend de plus en plus de la technologie, et dans ce cadre, il me semble fondamental que les gens aient un contrôle dessus. Vous devriez avoir la possibilité de savoir ce que fait la technologie que vous utilisez et également d'être en mesure d’influencer son fonctionnement. Et cette conception est au cœur des logiciels libres.

Malheureusement, cette préoccupation n’est pas au cœur de la plupart des technologies avec lesquelles vous interagissez au quotidien, bien au contraire. Hormis de rares exceptions, partout vous ne trouverez que chasses gardées et cadenas. »

« Les équipes de KDE œuvrent avec persistance pour vous fournir des technologies sur lesquelles vous avez le contrôle. Vous pouvez savoir ce qu'elles font à n'importe quel moment. De cette façon, vous êtes responsables de votre utilisation des technologies, de vos données et de votre vie – un socle qui définit de la liberté pour un grand nombre de personnes. Ceci est écrit dans la première phrase de notre manifeste : « Nous sommes une communauté de technologues, de designers, d’écrivains et d’avocats qui œuvrons afin de garantir la liberté pour tout un chacun au travers de nos logiciels. »

Envie de [nous rejoindre][3] afin d'agir pour permettre à toujours plus de personnes d'utiliser les technologies libres ? C’est aujourd’hui, c'est maintenant ! »

Article rédigé par Jonathan Riddell ([Source][4]) et traduit en français par Sophie Abbad

[1]: https://userdatamanifesto.org/
[2]: http://blog.lydiapintscher.de/2015/09/03/kde-signs-the-user-data-manifesto-2-0-and-continues-to-defend-your-freedom/
[3]: https://community.kde.org/Get_Involved
[4]: https://dot.kde.org/2015/10/07/kde-signs-user-data-manifesto-20
