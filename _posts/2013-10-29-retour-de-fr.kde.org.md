---
layout: post
title:  "fr.kde.org est de retour"
date:   2013-10-29 11:17:58
categories: KDE
---

Après un long silence, [fr.kde.org](http://fr.kde.org) est de retour ! Sur ce
site vous trouverez entre autres, une présentation des [différents espaces de
travail][espaces], et d'[applications][app] réalisés par la communauté KDE, des
informations sur les [différentes façons de nous rejoindre][communaute] ainsi
que des annonces des événements KDE francophone.

[espaces]: /espaces-travail
[app]: /applications
[communaute]: /communaute
