---
layout: post
title:  "Akademy 2014 à Brno"
date:   2014-09-11 12:00:00
categories: Akademy
---

Comme tous les ans, les contributeurs de KDE se retrouvent une semaine pour des conférences, des échanges et des sprints de développement. Cet évènement annuel, appelé [Akademy][1], s'est déroulé cette année à Brno, en République Tchèque du 6 au 12 septembre 2014.

Après deux jours de conférences (résumé [jour 1][2] et [jour 2][3]) autour de nombreux thèmes (communauté, technique, artistiques etc.), Akademy s'est poursuivie avec des conférences plus informelles et des groupes de travail dans une excellente ambiance avec des retrouvailles et l'accueil chaleureux des nouveaux contributeurs.

<a href="https://akademy.kde.org/2014">
<img src="https://dot.kde.org/sites/dot.kde.org/files/akademy2014_group_photo-wee.png"
alt="Une belle brochette de geeks" width="750px" />
</a>


[1]: https://akademy.kde.org/2014
[2]: https://dot.kde.org/2014/09/07/akademy-2014-day-1
[3]: https://dot.kde.org/2014/09/08/akademy-2014-day-2-talks

