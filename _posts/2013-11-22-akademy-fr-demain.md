---
layout: post
title:  "Akademy-fr, ça commence demain !"
date:   2013-11-22 14:28:00
categories: KDE
---

Demain aura lieu l'édition 2013 d'[Akademy-fr][akfr] au sein de [Capitole du
libre][cpdl]. Si vous êtes sur Toulouse, vous êtes cordialement invités à venir
découvrir les produits et la communauté KDE. C'est l'occasion d'assister à des
conférences (samedi), de participer à des ateliers (dimanche) ou tout simplement
de venir discuter avec les membres de la communauté KDE.

Comme chaque année, le programme mélange sujets techniques et grand public,
permettant à chacun de trouver chaussure à son pied.

[akfr]: http://2013.capitoledulibre.org/akademy-fr.html
[cpdl]: http://2013.capitoledulibre.org
