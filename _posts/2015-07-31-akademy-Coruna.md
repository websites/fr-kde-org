---
layout: post
title:  "Akademy 2015 à La Corogne"
date:   2015-07-31 12:00:00
categories: Akademy
---

L'évènement annuel autour de KDE, appelé [Akademy][1], a eu lieu cette année à La Corogne en Espagne.

C'est ainsi que du 25 au 31 Juillet 2015 se sont enchainées des conférences avec notamment l'annonce du très prometteur [Plasma Mobile][2] et une pléthore d'intervention autour de thèmes très variés (communauté, technique, artistique etc.), vous pouvez trouver les résumés (en anglais) [jour 1][3] et [jour 2][4].

Par la suite des groupes de travail ont été mis en place afin de pouvoir faire évoluer les différents produits de la communauté KDE dans une excellente ambiance.

Pour les francophones le prochain gros évènement sera Akademy-FR qui aura lieu les 21 et 22 Novembre à Toulouse (hébergé une nouvelle fois par le Capitole du Libre). 

<a href="https://akademy.kde.org/2015">
<img src="https://dot.kde.org/sites/dot.kde.org/files/20027702985_0120744d4c_c.jpg"
alt="Une belle brochette de geeks" width="750px" />
</a>


[1]: https://akademy.kde.org/2015
[2]: http://plasma-mobile.org
[3]: https://dot.kde.org/2015/07/25/akademy-talks-day-1
[4]: https://dot.kde.org/2015/07/26/akademy-talks-day-2

