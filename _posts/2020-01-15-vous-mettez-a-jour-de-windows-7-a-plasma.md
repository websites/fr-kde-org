---
title: >
  Vous effectuez une mise à jour de Windows 7 à Plasma ? Faites-le de la
  manière la plus simple ! 
layout: post
---

<figure class="img-fluid text-center">
  <img src="https://dot.kde.org/sites/dot.kde.org/files/Screenshot_20200115_000427.jpg"
    alt="Le bureau Plasma avec un theme resemblant à Windows 10"
    class="img-fluid"/>
  <figcaption>Le bureau Plasma avec un thème ressemblant à Windows 10.</figcaption>
</figure>

**Aujourd'hui, Microsoft ne fournira plus de mise à jour pour Windows 7.**

La firme de Redmond ne fournira plus de mises à jour pour le système
d'exploitation datant de 2009. Cela met près d'un milliard de personnes dans
la situation difficile de devoir faire face à des risques de sécurité accrus
parallèlement à une lente diminution de la disponibilité des logiciels.

Les gens qui rejettent les mises à jour forcées de Microsoft choisissent déjà
de reprendre le contrôle de leurs systèmes en passant au
[bureau Plasma](https://kde.org/plasma-desktop), construit sur une
philosophie centrée sur la liberté et le respect de ses utilisateurs. Le buzz
récent sur la [mise à jour vers Plasma](https://dot.kde.org/2020/01/08/plasma-safe-haven-windows-7-refugees)
a apporté beaucoup de nouveaux visages à bord, et maintenant ils essaient de
naviguer dans un nouveau système d'exploitation qui, même si similaire à Windows,
a ses différences.

Si vous êtes l'une de ces personnes, vous vous demandez probablement où vous
pouvez trouver des utilisateurs expérimentés pour vous aider à installer Plasma.

## Comment faire le saut avec facilité ?

Heureusement, il y a une foule de ressources disponibles pour les nouveaux
venus dans le monde de Plasma et Linux.

Le meilleur endroit pour parler en direct avec les utilisateurs de Plasma,
poser des questions et connaître la communauté KDE est le chat [#kde-welcome]()
sur Matrix pour les anglophones et [#kde-fr]() sur Matrix et IRC pour les
francophones. Si vous voulez discuter de Plasma et commenter sur les dernières
nouvelles de KDE avec d'autres utilisateurs, vous pouvez le faire sur
le subreddit [r/kde](https://www.reddit.com/r/kde/) sur Reddit ou consultez
les [forums officiels de KDE](https://forum.kde.org).

Si vous préferez poser vos questions en français, il existe également le forum
de [linux.fr](https://linuxfr.org/forums), ainsi que le forum de la communauté
francophone d'Ubuntu: [ubuntu-fr](https://forum.ubuntu-fr.org/). Il existe
aussi des Groupes d'Utilisateurs Locaux de Linux (GULL), que l'on peut trouver par
example sur [cette liste](https://www.agendadulibre.org/orgas).

Au niveau anglophone, [AskUbuntu.com](https://askubuntu.com/) est le plus grand
site de support technique dédié dans le monde Linux, et une ressource inestimable
pour quiconque utilise KDE Neon ou Kubuntu. La plupart des informations disponibles
sur ce site sont compatibles avec d'autres distributions Linux. D'autres endroits
pour des questions de support spécifiques incluent
[r/Linux4Noobs](https://reddit.com/r/Linux4Noobs) et [r/LinuxQuestions](https://reddit.com/r/LinuxQuestions)
on Reddit. En parlant de cela, une autre grande ressource est le forum
[Linux Questions](https://www.linuxquestions.org/).

Une fois que vous aurez un peu d'expérience, si vous rencontrez des problèmes avec
un composant système spécifique, vous pouvez toujours recourir au
[wiki d'Arch Linux](https://wiki.archlinux.org/) qui est aussi traduit en
[français](https://wiki.archlinux.fr/Accueil). La documentation fournie dans ce wiki
est d'excellente qualité et est souvent utile pour tous les utilisateurs de Linux.

Tout le monde dans la communauté KDE est familier avec les obstacles auxquels les
nouveaux utilisateurs font face lorsqu'ils font le saut vers Plasma et le monde
du logiciel libre. N'hésitez pas à profiter de l'accueil de la communauté KDE qui
vous aidera à vous sentir chez vous dans Plasma et s'assurera que vous tirez le
meilleur parti de votre système nouvellement mis à niveau.

[Article original](https://dot.kde.org/2020/01/14/moving-windows-7-plasma-do-it-easy-way).
Traduit et adapté de l'anglais
