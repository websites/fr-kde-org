---
layout: post
title:  "Aidez KDE, Campagne de levée de fond"
date:   2015-08-18 10:00:00
categories: KDE
---

Chaque année la communauté KDE organise des sprints, réunions physiques, afin de pouvoir avancer sur ses différents projets de manière plus productive.

Pour la 6ème année, les développeurs KDE se réuniront à Randa (Suisse) pour réaliser plusieurs sprints sur différents projets (KDE Connect, KDE sur Android, digiKam, GCompris, Kdenlive, Design de l'interface, la sécurité, etc.)

Grâce aux sprints de Randa et aux autres sprints organisés tout au long de l'année, la communauté KDE peut ainsi vous proposez les meilleurs outils pour l'utilisation de votre ordinateur dans des domaines très variés : l'édition vidéo avec Kdenlive, la gestion de vos photos avec DigiKam, les logiciels éducatifs (GCompris et l'ensemble de KDEEdu), Plasma5 le bureau KDE ou encore Plasma Mobile la version pour smartphone annoncé durant Akademy, et bien d'autres encore.

La contrepartie de l'organisation d'un sprint est le coût. En effet regrouper les différents développeurs, designers, graphistes, etc. lors d'un sprint nécessite le déplacement et l'hébergement de ceux-ci.

C'est pourquoi une campagne de levée de fond a lieu afin de pouvoir financer l'ensemble des sprints KDE.
Comme vous l'avez compris, c'est un moyen de s'impliquer dans KDE en finançant son avenir très prometteur.

Pour plus d'informations et participer: [KDE Sprints 2015 fundraising][1]

[1]: https://www.kde.org/fundraisers/kdesprints2015/
