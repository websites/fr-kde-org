---
layout: post
title:  "Compilation des logiciels de KDE 4.12"
date:   2013-12-18 11:26:00
categories: KDE
---

> La communauté KDE est fière d'annoncer les mises à jour majeures des
> applications KDE avec de nouvelles fonctionnalités et de nombreuses corrections
> de bogues. Le bureau Plasma et la plate-forme KDE n'ont pas évolués en dehors
> de corrections d'anomalies. Les équipes se concentrent sur la transition
> technique vers la plate-forme en version 5. La mise à jour du numéro de version
> est donc avant tout une facilité pour la gestion des paquets. Toutes les
> corrections d'anomalies et les nouvelles fonctionnalités des applications, du
> bureau Plasma et de la plate-forme 4.11 ont été incluses.

Plus de détails dans l'[annonce officielle][1] sur le site de KDE.

[1]: http://www.kde.org/announcements/4.12/index.php?site_locale=fr
