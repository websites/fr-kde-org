---
layout: post
title:  "Interview: Timothée Giet"
date:   2015-08-10 11:20:00
categories: Interview
---

Afin de vous présenter les contributeurs KDE francophone une série d'interview va être publié au cours des prochains mois. Nous commençons par Timothée Giet connu pour son implication dans Krita. Il a ainsi obtenu un *Jury Award* lors d'Akademy 2013, *For shaping the future and community of Krita*.

**Salut Timothée peux-tu te présenter brièvement**  
Alors en bref, je suis illustrateur/infographiste indépendant. Je travaille uniquement avec des logiciels libres depuis 2010. Je contribue sur plusieurs projets de logiciels libres dont principalement le logiciel de dessin Krita.


**Quand as tu entendu parler de KDE la première fois ?**  
Lorsque j'ai commencé à utiliser un système d'exploitation GNU/Linux, autour de 2008 je crois. A cette époque j'ai testé bon nombre de distributions et d'environnements de bureaux différents, dont celui de KDE.

**Ton choix d'utiliser celui de KDE est lié à l'utilisation de Krita ?**  
Plus ou moins. Au départ, après avoir testé les bureaux existants à l'époque, j'étais resté sur Gnome2 car il correspondait à mon utilisation de l'époque et était assez léger pour ma machine. Ensuite, lorsque j'ai commencé à utiliser Krita, j'ai voulu essayer de l'utiliser dans son environnement natif. J'ai donc commencé à basculer sur le bureau de KDE.

**Peux-tu nous en dire plus sur Krita, pourquoi ce choix ?**  
A l'époque, les seuls outils de dessins libres disponibles étaient Gimp et Mypaint, tous deux très intéressants mais je m'y sentais à l'étroit. Lorsque j'ai découvert Krita, avant la version 2.3, j'ai trouvé un logiciel très instable, mais avec un potentiel créatif très intéressant et une petite équipe de développeurs à la recherche d'utilisateurs.
J'ai donc décidé de l'essayer, ce qui m'a très rapidement emmené à compiler la version de développement pour participer en reportant les bugs et en testant les changements au fur et à mesure.

![Illustration réalisée par Timothée à l'aide de Krita](/images/posts/2015-08-10/28-BirdyGirl_web.png "Illustration réalisée par Timothée à l'aide de Krita")

**Après tes premières contributions (rapport de bug), tu as collaboré plus étroitement avec l'équipe de Krita, peux tu nous en dire plus ?**  
A ce moment là, comme les versions disponibles n'étaient pas réellement utilisables, il n'y avait quasiment aucun utilisateur sérieux pour rapporter les problèmes existants. J'ai donc décidé d'utiliser le logiciel sur des projets de complexité croissante, ce qui a permis de stabiliser le logiciel progressivement jusqu'à un état suffisant pour attirer d'autres utilisateurs.
A partir de là, de plus en plus d'autres utilisateurs ont commencé à contribuer sur ces tâches, ce qui m'a permis d'utiliser mon temps sur d'autres tâches. 

**Krita utilisable quel est devenu ton rôle au sein du projet ?**  
Comme j'avais commencé à m'occuper des ressources utilisateurs intégrées par défaut, je me suis rapidement retrouvé à coordoner les contributions à ce niveau. Ensuite, lorsqu'a démarré le projet krita-sketch (version simplifiée avec une interface graphique tactile), je me suis occupé de l'habillage de l'interface graphique. J'ai eu l'occasion de faire des démonstrations du logiciel sur de nombreux évènements.
Depuis j'ai fait toutes sortes de contributions, y compris plusieurs lignes de code C++. Je finis actuellement l'intégration de nouvelles icones.

**Récemment, une campagne de financement participatif a été faite autour du projet GCompris afin que tu puisses faire une refonte graphique de celui-ci. Comment t'es venu cette idée et le résultat est-il un échec pour toi ?**  
L'idée est née lors de ma rencontre avec Bruno, créateur et mainteneur du projet GCompris, lors de la réunion de la communauté KDE Akademy en 2014. Je connaissais déjà ce logiciel éducatif, mais j'ai toujours trouvé dommage qu'il ne bénéficie pas de graphismes attrayant pour le mettre en valeur.
J'ai donc discuté de ce problème avec Bruno, et l'idée du projet est née.
Je ne vois pas le résultat comme un échec, au contraire. Certes nous n'avons pas atteint l'objectif permettant de tout faire du premier coup, mais c'était un résultat attendu. L'objectif étant relativement élévé, il était difficile de l'atteindre en une fois, c'est pourquoi nous avons opté pour un financement modulable permettant de réaliser une partie du travail en fonction du montant récolté.
En pratique, cela a permis d'établir de bonnes bases avec la charte graphique intégrée dans le menu et une bonne partie des activités.
Pour la suite, nous avons prévu de lancer une seconde campagne de financement très bientôt pour continuer ce projet.

**Tes contributions à KDE sont donc principalement sur Krita et GCompris. Fais tu d'autres contributions ?**  
Il y a quelques temps déjà, j'ai réalisé des icônes d'applications pour la suite Calligra dont fait partie Krita.
Plus récemment, j'ai dessiné des illustrations pour les cartes postales de la campagne de financement de KDE en fin d'année dernière.
Mes contributions sont donc principalement sur ces deux projets pour l'instant, mais pas exclusivement.

**Tu es récemment passé à Plasma 5, quelles sont tes premières impressions ?**  
Plasma 5 me plait beaucoup. Je l'utilise sur mon poste de bureau sur lequel il tourne plutôt bien. Cependant j'ai pu remarquer que la stabilité de ce bureau dépend beaucoup de la combinaison carte-graphique/driver utilisée pour l'instant, rendant le bureau inutilisable dans les pires des cas. Il semble que ces problèmes sont principalement des bugs de drivers exposés par ce logiciel, j'espère donc qu'ils seront rapidement corrigés pour que tout le monde puisse profiter de ce bureau moderne.

**Qu'est ce qui te permet de garder de la motivation pour contribuer ?**  
Le concept de partage des connaissances et de la culture associé aux logiciels libres est la raison principale qui m'a attiré et qui me donne toujours l'envie de contribuer, que ce soit dans mon travail ou sur mon temps libre.

**As tu prévu de participer à des évènements autour du logiciel libre dans les mois qui viennent ?**  
Pour la fin de l'année, j'ai prévu de participer au Capitole du Libre, ou se tiendra également la réunion Akademy-Fr de la communauté KDE francaise, à Toulouse fin novembre. Je compte proposer une ou deux présentations, mais je préfère rester silencieux sur le sujet tant que rien n'est confirmé.

**As tu autre chose à ajouter ?**  
Je voudrais juste remercier tous les contributeurs de logiciels libres qui rendent possible cette alternative grace à leurs efforts combinés, et notamment la communauté KDE.

**Merci à toi, je te souhaite beaucoup de réussite dans tout ce que tu entreprends**  
Merci!

Si vous voulez suivre l'activité de Timothée, rendez-vous sur son [blog][1]

Interview par Benjamin Port pour **fr.kde.org**

[1]: timotheegiet.com/blog/
