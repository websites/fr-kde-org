---
layout: post
title:  "Priorité au logiciel libre"
date:   2014-02-23 12:00:00
categories: KDE
---

Afin de donner la priorité au logiciel libre, KDE soutient pleinement [l'April][1] (Association pour la promotion et la recherche en Informatique Libre)
dans ses actions.
Aujourd'hui, l'April mène une campagne d'adhésion afin d'augmenter sa capacité d'action et promouvoir la liberté informatique.

<a href="http://april.org/campagne">
<img width="374" height="48"
src="//april.org/campagne/images/priorite-logiciel-libre-je-soutiens-april_m.png"
alt="Priorité au Logiciel Libre! Je soutiens l'April." />
</a>

Plus de détails sur le site [la campagne de l'April][2].

[1]: http://www.april.org/
[2]: http://www.april.org/campagne/
