---
title: "Plasma : un refuge pour les utilisateurs de Windows 7"
layout: post
date:   2020-01-10 9:00:00
categories: KDE
---
<figure class="text-center">
  <img class="img-fluid" alt="L'environment de bureau Plasma complétement fonctionnelle avec un theme Windows 7"
    src="https://dot.kde.org/sites/dot.kde.org/files/Screenshot_20200108_105524.png" />
  <figcaption>L'environment de bureau Plasma complètement fonctionnel avec un thème Windows 7</figcaption>
</figure>

À partir du 14 janvier 2020, Microsoft va arrêter de fournir des mises à jour pour Windows 7.

Il n'y aura donc plus de correctifs pour corriger les bugs et les failles de sécurité.
Cela laissera les utilisateurs de Windows 7 exposés à toutes sortes de nombreux problèmes. Mais
ce n'est pas un gros souci pour Microsoft. Avec ce changement, la firme de Redmond espère
encourager les utilisateurs à migrer à Windows 10.

Mais pourquoi devrait-on s'en soucier ? Peut-être parce que Windows détient actuellement 77%
de la part de marché mondiale des ordinateurs de bureau (tous les ordinateurs de bureau Linux
combinés en détiennent moins de 2%). De ces 77%, près de 30% appartiennent à Windows 7. C'est
près d'un milliard de personnes qui s'accrochent encore à Windows 7 parce qu'elles résistent
au passage à Windows 10. En plus de la résistance naturelle de l'homme au changement, Windows
10 s'est mérité une mauvaise réputation en tant que système d'exploitation qui laissera
[fuiter volontiers vos données vers Microsoft](https://www.howtogeek.com/224616/30-ways-windows-10-phones-home/)
et [exposera votre bureau à de nombreuses publicités intrusives](https://www.theverge.com/2017/3/17/14956540/microsoft-windows-10-ads-taskbar-file-explorer).

Aider les gens à reprendre le contrôle de leurs systèmes et protéger leurs données est
précisément ce que les communautés de logiciel libre font de mieux. C'est donc l'occasion
parfaite pour aider les utilisateurs de Windows 7 à passer à quelque chose de bien meilleur :
à l'[environnement de bureau Plasma](https://kde.org/plasma-desktop) !

## Comment vous pouvez aider ?

Nous avons besoin de votre aide pour convaincre les utilisateurs de Windows 7 de passer au
bureau Plasma. Nous avons créé une [tâche où nous faisons un brainstorming d'idées, de
conseils et de ressources](https://phabricator.kde.org/T12444). Vous pouvez également nous
faire part de vos réflexions. Obtenez votre identité KDE aujourd'hui et rejoignez la conversation.

Vous pouvez également [rejoindre l'équipe de promotion directement sur Matrix](https://webchat.kde.org/#/room/#kde-promo:kde.org) et nous aider
à mener cette campagne.

Ou voler en solo ! Parlez à vos amis, votre famille, vos camarades de classe et vos
collègues. Même si vous convainquez une seule personne de faire la transition vers
n'importe quel système basé sur Linux, vous aurez fait quelque chose de précieux et aidé
le mouvement du logiciel libre.

<div class="embed-responsive embed-responsive-16by9">
<iframe lass="embed-responsive-item" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/230457b7-ebd2-4058-a23b-24d08201a9e3?start=2s" frameborder="0" allowfullscreen></iframe>
</div>

Le thème de type Windows 7 présenté ci-dessus a été assemblé (à partir de nombreux components
créés par de généreux contributeurs) par Dominic Hayes, créateur de [Feren OS](https://ferenos.weebly.com/), une distribution
Linux basée sur Ubuntu et destinée aux utilisateurs finaux. Regardez ça !

Pour vous aider à faire un choix pour une distribution Linux avec Plasma préinstallé,
nous avons créé une [liste](https://kde.org/distributions).

[Article orginal en anglais](https://dot.kde.org/2020/01/08/plasma-safe-haven-windows-7-refugees)
